# docker-compose

## Initialize

```bash
npm install
```

run `sudo docker-compose up` in a terminal

if pb:
sudo rm /var/run/docker.pid
sudo rm /var/run/docker.sock

### Redis DataBase

Connect to the container in "interactive mode":

```bash
docker exec -it redis-server /bin/sh
```

Then, when you get the `#` prompt, launch the Redis Client with the following command:

```bash
redis-cli
```

You should get a new prompt `127.0.0.1:6379>`, then type `ping` and you should get a `PONG` response from the server:

```bash
127.0.0.1:6379> ping
PONG
```
> Type `exit` to quit the Redis Client, then `exit` again to quit the interactive mode.

#### How to remote connect to the Redis database

If you installed the Redis client on the host machine, you can use this command:

```bash
redis-cli -h localhost -p 6379
```

**Remark**: you remember that you updated your `hosts` file, so you can use `redis-server` instead of `localhost` or `127.0.0.1`:

```bash
redis-cli -h redis-server -p 6379
```

> `6379` is the default Redis port

### MQTT

```bash
node listen.js
```

```bash
node publish.js
```
const mqtt = require('mqtt')
const client  = mqtt.connect('mqtt://localhost:1883')

client.on('connect', _ => {
  client.subscribe('house', (err) => {
    console.log(err ? err: "👋 Subscribed to house/#")
  })
})

client.on('message',  (topic, message) => {
  console.log("You've got a message on topic:", topic)
  console.log("message:", message.toString())
})
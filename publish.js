const mqtt = require('mqtt')
const client  = mqtt.connect('mqtt://localhost:1883')

client.on('connect', _ => {
  client.subscribe('house', (err) => {
    if (!err) {
      client.publish('house', JSON.stringify({function:"🖐️ hello", params:{name:"😃 bob"}}))
      client.publish('house', JSON.stringify({message:"👋 hello world 🌍"}))
    }
  })
})

client.on('message',  (topic, message) => {
  console.log("👋",JSON.parse(message.toString()))
  client.end()
})
